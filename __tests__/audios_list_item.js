import React from 'react';
import AudiosItem from '../src/components/audios/audios_item';
import Styles from '../src/styles/style';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme from 'enzyme';
Enzyme.configure({ adapter: new Adapter() });
import SeekBar from '../src/components/audios/seek_bar';

const {  
  Text,
  View,
  Image,
  ListView
} = require('react-native');

describe('renders audios item correctly', () => {
	const navigation = { navigate: jest.fn() };
	const audiosItemWrapper = renderer.create(<AudiosItem navigation={navigation}/>);

	navigation.getParam = function(){
		return props;
	}	

	var props = {
		title : 'test!',
		cover : 'https://raw.githubusercontent.com/Learnfield-GmbH/CodingChallange/master/data/Oceansound.png',
		audio : 'https://raw.githubusercontent.com/Learnfield-GmbH/CodingChallange/master/data/Oceansound.mp3'
	}

    it('snapshot test audios item ', () => {
        const audiosItemWrapper = renderer.create(<AudiosItem navigation={navigation}/>).toJSON();
  		expect(audiosItemWrapper).toMatchSnapshot();

  		
    })

    it('renders a audios item elements', () => {   	
		const audiosItemWrapper = shallow(
			<AudiosItem navigation={navigation}/>
		);

		expect(audiosItemWrapper.contains(
			<Text style={Styles.headerText}>
			  test!
			</Text>
		)).toBe(true);	    
	});

	it('check number of images', () => {   			
		expect(audiosItemWrapper.root.findAllByType(Image)).toHaveLength(2);    
	});

	it('check slider redered correctly', () => { 
		expect(audiosItemWrapper.root.findAllByType(SeekBar)).toHaveLength(1);    
	});
})



