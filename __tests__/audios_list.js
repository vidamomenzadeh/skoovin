import React from 'react';
import AudiosList from '../src/components/audios/audios_list';
import Styles from '../src/styles/style';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme from 'enzyme';
Enzyme.configure({ adapter: new Adapter() });

const {  
  Text,
  View,
  ListView
} = require('react-native');

describe('renders audios list correctly', () => {
    it('snapshot test audios list', () => {
        const audiosListWrapper = renderer.create(<AudiosList />).toJSON();
  		expect(audiosListWrapper).toMatchSnapshot();
    })

    it('renders a audios list elements', () => {
    	
		const audiosListWrapper = shallow(
			<AudiosList />
		);

		expect(audiosListWrapper.contains(
			<Text style={Styles.headerText}>
			  Skoovin
			</Text>
		)).toBe(true);

		expect(audiosListWrapper.find(
			<ListView style={Styles.listViewWrapper}>
			
			</ListView>
		)).toBeDefined(); 
 
	});
})



