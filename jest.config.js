
module.exports = {
	"preset": 'react-native',
    "modulePaths": [
      "<rootDir>/node_modules"
    ],
    "transform": {
    "^.+\\.js$": "<rootDir>/node_modules/react-native/jest/preprocessor.js"    
    }
};