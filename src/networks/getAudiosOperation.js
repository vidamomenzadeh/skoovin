import Config from './config.json';

var React = require("react-native");
import OperationService from './operation';
const operation = new OperationService(); 

export default class GetNewsOperation { 
  constructor() {   
    this.getAudios = this.getAudios.bind(this);
  }

  getAudios() { 
   return operation.fetch(``, false, {
      method: 'GET',     
    });
  }
}