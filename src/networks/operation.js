import Config from './config.json';
import DefaultPreference from 'react-native-default-preference';

var React = require("react-native");

var {   
    AsyncStorage
} = React;

export default class OperationService {
  constructor(navigation) {
    
    this.domain = Config["appServerRootURL"];
    this.fetch = this.fetch.bind(this);  
  }

  async _checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
      return response
    } else {

      if(response.status == 401){
         // Logout User
      }

      var error = new Error(response.statusText)
      error.response = response;
      throw error
    }
  }

  async fetch(url, signRequest, options){
    url = this.domain + url;    
    const headers = {
      'Content-Type': 'application/json'
    }

    if(signRequest){
        const isLoggedIn = await this._loggedIn();

        if (!isLoggedIn){        
          await getRefreshTokenOperation._getRefrestToken();
        }

        var token =  await DefaultPreference.get('TOKEN');
        headers['Authorization'] = 'Bearer ' + token;
    }    
   
    return fetch(url, {
      headers,
      ...options
    })
    .then(this._checkStatus)    
    .then((response) => response.json())
    .then((responseJson) => {      
      return responseJson;
    }) 
  }
}