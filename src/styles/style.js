import { StyleSheet } from 'react-native';
export default StyleSheet.create({
   container: {
    flex: 1,
    padding : 30
  },

   loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10
  },

  headerText:{
   textAlign: 'center'
  },

  containerListItem:{
  	marginTop:20
  },

  playBtn:{
  	justifyContent: 'center',
    alignItems: 'center',
    marginTop:20
  },

  sliderTime:{
  	textAlign: 'center'
  },

  containerListItemTitle:{
  	margin:10,
  	textAlign: 'center'
  },
  loadingMask:{
      justifyContent: 'center', 
      alignItems: 'center',
      position : 'absolute', 
      top : 0, 
      bottom : 0, 
      right : 0, 
      left : 0, 
      backgroundColor : 'rgba(0,0,0,0.3)'
  }
});