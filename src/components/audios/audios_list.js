import React, { Component } from 'react';
import Styles from '../../styles/style';

import GetAudiosOperation from '../../networks/getAudiosOperation';
const getAudiosOperation = new GetAudiosOperation();
import ImageScale from 'react-native-scalable-image';
const {
  AppRegistry,
  Dimensions,
  StyleSheet,
  TouchableHighlight,
  ScrollView,
  Text,
  TextInput,
  Image,
  ActivityIndicator,
  ListView,
  View 
} = require('react-native');

const LoadingIndicator = ({ loading }) => (
  loading ? (
    <View style={ Styles.loadingMask }>
      <ActivityIndicator
        animating={ true }
        color="#000"
        size="large"/>
    </View>
  ) : null
)

export default class AudiosListScreen extends Component<{}> {

  constructor(props) {
      super(props);

      const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
      this.state = {
        page: 1,
        audios: [],
        dataSource: ds,
        totalPage : 1
      }     
  };  

  componentDidMount() {   
     this._getAudios();
  };

  _getAudios() {
    getAudiosOperation.getAudios()
     .then(responseJson => {      
        const audios = this.state.page === 1 ? responseJson.data : [ ...this.state.audios, ...responseJson.data ]
        this._update(false, audios);
    }).catch((error) =>{
        console.error(error);
    });    
  }

  _update(loading, audios = null) {
    const loadingRow = {
      type: 'Loading',
      loading: loading
    }

    this.setState({
      audios: audios,
      dataSource: this.state.dataSource.cloneWithRows([ ...audios, loadingRow ])
    })
  }

  _renderRow=(row)=>{
     if (row.type === 'Loading') {
      return <LoadingIndicator loading={ row.loading } />
    } else {
      return (
        <TouchableHighlight
            underlayColor="transparent"
            onPress={() =>  this.props.navigation.navigate('AudioItemScreen', {
              data: row
            })}>
          
          <View style={Styles.containerListItem}>                      
               <ImageScale
                width={Dimensions.get('window').width - 60}
                source={{uri: row.cover}}/> 

                <Text style={Styles.containerListItemTitle}>
                    {row.title}
                </Text>                    
          </View>                                  
      </TouchableHighlight>);
    }
  }

  render() { 

    return (
      <ScrollView style={Styles.container}>
        <View> 
            <Text style={Styles.headerText}>
              Skoovin
            </Text>
        </View> 
         <ListView
          style={Styles.listViewWrapper}
          enableEmptySections={ true }
          automaticallyAdjustContentInsets={ false }
          renderSeparator={(sectionId, rowId) => <View key={rowId} style={Styles.separator} />}
          dataSource={this.state.dataSource}
          renderRow={this._renderRow}/>
  
      </ScrollView>    
    );
  }
}



