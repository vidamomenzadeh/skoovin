import React from "react";
import Slider from "react-native-slider";
import {StyleSheet, View, Text } from "react-native";
import Styles from '../../styles/style';
import {FORMAT_TIME} from '../../constant/helper';

export default class SeekBar extends React.Component {
  state = {
    value: 0
  };

  render() {
    const { duration, currenttime, changeCurrentTime } = this.props;

    return (
      <View style={Styles.container}>
        <Slider
           maximumValue={Math.round(duration)}
           value={currenttime}
           step = {1}         
           onSlidingComplete={changeCurrentTime}/>
        <Text style={Styles.sliderTime}>
          {FORMAT_TIME(currenttime * 1000)} / {FORMAT_TIME(duration * 1000)}
        </Text>
      </View>
    );
  }
}

