import React, { Component } from 'react';
import Styles from '../../styles/style';
import ImageScale from 'react-native-scalable-image';

import Controls from './controls';

const {
  AppRegistry,
  Dimensions,
  StyleSheet,
  TouchableHighlight,
  ScrollView,
  Text,
  TextInput,
  Image,
  ListView,
  ActivityIndicator,
  View 
} = require('react-native');

const LoadingIndicator = ({ }) => (
 
    <View style={ styles.loading }>
      <ActivityIndicator
        animating={ true }
        style={[ styles.loading ]}
        size="large"/>
    </View>
  
)

export default class AudioItemScreen extends Component<{}> {

  constructor(props) {
      super(props);

      this.state = {
        loading : true
      }

  };  

  render() { 
    const { navigation } = this.props;
    const data = navigation.getParam('data');

      return(    
 
          <ScrollView style={Styles.container}>
            <View >
                <Text style={Styles.headerText}>
                 {data.title}
                </Text>   

                <ImageScale
                    style={Styles.containerListItem}
                    width={Dimensions.get('window').width - 60}
                    source={{uri: data.cover}}/>

                <Controls url={data.audio}/>
                 
            </View> 
            
          </ScrollView>    
      
        );  
      } 
    
  
}





 


