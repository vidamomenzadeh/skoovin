
import React from "react";
import Slider from "react-native-slider";
import {StyleSheet, TouchableHighlight, View, Text, Image, ActivityIndicator } from "react-native";
import AudioPlayer from 'react-native-play-audio';
import SeekBar from './seek_bar';
import Styles from '../../styles/style';

const LoadingIndicator = ({ }) => ( 
    <View style={ Styles.loadingMask }>
      <ActivityIndicator
        animating={ true }
        color="#fff"
        style={[ Styles.loadingMaskIcon ]}
        size="large"/>
    </View>  
)

export default class Controls extends React.Component {  
  constructor(props){
   super(props)

    this.state = {
      status : 'play',
      duration : 0,
      currenttime : 0,
      loading     : true
    };   

    this.changePlayStatus = this.changePlayStatus.bind(this);
    this.onEndOfPlay = this.onEndOfPlay.bind(this);
    this.getDuration = this.getDuration.bind(this);
    this.getCurrentTime = this.getCurrentTime.bind(this);
    this.changeCurrentTime = this.changeCurrentTime.bind(this);
  }

  componentWillUnmount (){
    clearInterval(this.timerIntervalId);
  }

  changePlayStatus(){
      var $this = this;
      clearInterval(this.timerIntervalId);

      if(this.state.status == 'play'){
          AudioPlayer.play();
          this.timerIntervalId = setInterval(() => {            
            AudioPlayer.getCurrentTime((currentTime) => {
              $this.getCurrentTime(currentTime);
            });
          }, 1000);
      }        
      else{
          AudioPlayer.pause();
      }       

      this.setState((prevState, props) => ({
        status : prevState.status == 'play' ? 'pause' : 'play'
      }));
  }

  render(){  
    return(
     <View>
        
        <TouchableHighlight 
          onPress={this.changePlayStatus} 
          style={Styles.playBtn}  
          underlayColor="transparent">
        { this.state.status == 'play' ? 
            <Image source={require('../../resources/img/play.png')} />
          :          
            <Image source={require('../../resources/img/pause.png')} />   
        }         
        </TouchableHighlight> 
        <SeekBar changeCurrentTime = {this.changeCurrentTime} duration={this.state.duration} currenttime={this.state.currenttime}/>         
        {this.state.loading ? <LoadingIndicator /> : null}
      </View>
    )
  }

  changeCurrentTime(time){
    AudioPlayer.setTime(time);
    this.setState({
      currenttime : time
    })
  }

  onEndOfPlay(){   
    clearInterval(this.timerIntervalId); 
    this.setState((prevState, newEmployer) => ({
        status : 'play',
        currenttime : Math.round(prevState.duration)
    }), () => {
        
    }); 
  }

  componentDidMount(){
      const {url}= this.props; 
      var $this = this;     
      
      AudioPlayer && AudioPlayer.prepare(url, () => {
        $this.setState({
          loading : false
        });
        AudioPlayer.onEnd(() => {$this.onEndOfPlay();});
        AudioPlayer.getDuration((duration) => {         
          $this.getDuration(duration);
        }); 

      })
  }

  getCurrentTime(currentTime){
    this.setState({
      currenttime : currentTime
    })
  }

  getDuration(duration){
    this.setState({
      duration : duration
    })
  }  

}





