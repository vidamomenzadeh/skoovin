console.disableYellowBox = true;

import React from 'react';

import AudiosListScreen from './src/components/audios/audios_list';
import AudioItemScreen from './src/components/audios/audios_item';
import { createStackNavigator, createAppContainer } from "react-navigation";


const AppNavigator = createStackNavigator({
  AudiosListScreen: {screen: AudiosListScreen, navigationOptions: { header: null }},
  AudioItemScreen: {screen: AudioItemScreen, navigationOptions: { header: null }}
}, {
  initialRouteName : 'AudiosListScreen'
});

export default createAppContainer(AppNavigator);



